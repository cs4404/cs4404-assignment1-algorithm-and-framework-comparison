# imports
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn import linear_model
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, explained_variance_score

# data files
file1 = 'The SUM dataset, without noise.csv'
file2 = 'The SUM dataset, with noise.csv'

# regression model
linearRegression = linear_model.LinearRegression(fit_intercept = False)

num_columns = pd.read_csv(file1, sep=';', nrows=1).shape[1]  
length = num_columns

# arrays used to store data, targets and predicted values
x = [None]*9
y = [None]*9
yPredict10 = [linearRegression]*9
yPredict30 = [linearRegression]*9

# arrays to hold metrics
accuracy10 = [None]*9
accuracy30 = [None]*9
RMSE10 = [None]*9
RMSE30 = [None]*9
regression = [linearRegression]*9
regr = [linearRegression]*9

#training and testing values
xTrain10 = [None]*length
xTest10 = [None]*length
yTrain10 = [None]*length
yTest10 = [None]*length
xTrain30 = [None]*length
xTest30 = [None]*length
yTrain30 = [None]*length
yTest30 = [None]*length


# columns needed for predictions for features and targets
featureColsSUM = [1,2,3,4,6,7,8,9,10]
targetColsSUM = [11]
numbers = [100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000]

#read data in from file
def getData(fileX):
    _nrows = 100
    for i in range(0, 9):
        x[i] = pd.read_csv(fileX, delimiter = ';', nrows = _nrows, usecols = featureColsSUM)
        #print x[i]
        x[i]=x[i].values

        y[i] = pd.read_csv(fileX, delimiter = ';', nrows = _nrows, usecols = targetColsSUM)
        #print y[i]
        y[i]=y[i].values
        
        if i%2 == 0:
            _nrows = _nrows*5
        else:
            _nrows = _nrows*2
# end of function
        
def setTestAndTrainVals():
    for k in range(0, 9):
        xTrain30[k], xTest30[k], yTrain30[k], yTest30[k] = train_test_split(x[k], y[k],
              test_size=0.30, shuffle = False)
        xTrain10[k], xTest10[k], yTrain10[k], yTest10[k] = train_test_split(x[k], y[k],
              test_size=0.30, shuffle = False)
        #print "getting train/test data", k
# end of function
        
def fitRegression():
    #print xTrain[0]
    for i in range(0,9):
        regr[i] = linearRegression
        regr[i] = linearRegression.fit(xTrain30[i], yTrain30[i])
        yPredict30[i] = regr[i].predict(xTest30[i])
        yPredict10[i] = cross_val_predict(linearRegression, x[i], y[i], cv=10) 
    # end of function
        
def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)
    
getData(file2)
setTestAndTrainVals()
fitRegression()
    
for j in range (0, 9):
    # 10 fold cross validation
    accuracy10[j] = explained_variance_score(y[j], yPredict10[j])
    accuracy30[j] = explained_variance_score(yTest30[j], yPredict30[j])
    RMSE10[j] = mean_squared_error(y[j], yPredict10[j])
    RMSE10[j] = math.sqrt(RMSE10[j])
    RMSE30[j] = mean_squared_error(yTest30[j], yPredict30[j])
    RMSE30[j] = math.sqrt(RMSE30[j])
    #print 'Root Mean Square: ', RMSE[j]
    #print 'Variance with Kfold', variance[j]
    #print 'Score with Kfold', regression[j].score(xTest[j], Kfold[j])

# Plot outputs
    
print RMSE10
print accuracy10

print RMSE30
print accuracy30
#print RMSE
plt.plot(numbers, RMSE10, color='blue', linewidth=3)
plt.plot(numbers, accuracy10, color='green', linewidth=3)
plt.plot(numbers, RMSE30, color='blue', linewidth=3)
plt.plot(numbers, accuracy30, color='green', linewidth=3)

#print len(numbers)
#print len(RMSE)
plt.xticks()
plt.yticks()

plt.show()
