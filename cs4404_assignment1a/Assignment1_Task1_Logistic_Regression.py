#!/usr/bin/python 3.6.2
import numpy as np
import pandas as pd
from sklearn import linear_model, model_selection, metrics
 
class Logistic_Regression:
    def __init__(self):
#        self.sum_without_noise()
#        self.sum_with_noise()
#        self.ny_texi()
        self.online_news()
     
    def sum_without_noise(self):
        num_rows = 100
        data_columns = [1,2,3,4,6,7,8,9,10]
        target_columns = [12]
         
        for i in range (9):
            SUM_without_noise_data = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = data_columns, nrows = num_rows)
            SUM_without_noise_target = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = target_columns, nrows = num_rows)
             
            prediction = model_selection.cross_val_predict(linear_model.LogisticRegression(), SUM_without_noise_data, np.ravel(SUM_without_noise_target), cv=10)
             
            print("Accuracy Score %s - %s" % (str(i), metrics.accuracy_score( np.ravel(SUM_without_noise_target), prediction)))
            print("F1 Score %s - %s" % (str(i), metrics.f1_score( np.ravel(SUM_without_noise_target), prediction, average='weighted')))
             
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2
     
    def sum_with_noise(self):
        num_rows = 100
        data_columns = [1,2,3,4,6,7,8,9,10]
        target_columns = [12]
         
        for i in range (9):
            SUM_with_noise_data = pd.read_csv('SUM with noise.csv', delimiter = ";", usecols = data_columns, nrows = num_rows)
            SUM_with_noise_target = pd.read_csv('SUM with noise.csv', delimiter = ";", usecols = target_columns, nrows = num_rows)
             
            prediction = model_selection.cross_val_predict(linear_model.LogisticRegression(), SUM_with_noise_data, np.ravel(SUM_with_noise_target), cv=10)
             
            print("Accuracy Score %s - %s" % (str(i), metrics.accuracy_score( np.ravel(SUM_with_noise_target), prediction)))
            print("F1 Score %s - %s" % (str(i), metrics.f1_score(np.ravel(SUM_with_noise_target), prediction, average='weighted')))
             
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2
                 
    def ny_texi(self):
        num_rows = 100
        data_columns = [5,6,7,8]
        target_columns = [11]
         
        for i in range(10):
            ny_texi_data = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = data_columns,  nrows = num_rows)
            ny_texi_target = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = target_columns,  nrows = num_rows)
                       
            prediction = model_selection.cross_val_predict(linear_model.LogisticRegression(), ny_texi_data, np.ravel(ny_texi_target), cv=10)
             
            print("Accuracy Score %s - %s" % (str(i), metrics.accuracy_score( np.ravel(ny_texi_target), prediction)))
            print("F1 Score %s - %s" % (str(i), metrics.f1_score( np.ravel(ny_texi_target), prediction, average='weighted')))
           
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2
                 
    def online_news(self):
        num_rows = 100
        online_news_pop = pd.read_csv("OnlineNewsPopularity.csv")
        for i in range(6):
            online_news_data = online_news_pop.iloc[:num_rows, 1:-2]
            online_news_target = online_news_pop.iloc[:num_rows, -1]
             
            prediction = model_selection.cross_val_predict(linear_model.LogisticRegression(), online_news_data, np.ravel(online_news_target), cv=10)
             
            print("Accuracy Score %s - %s" % (str(i), metrics.accuracy_score( np.ravel(online_news_target), prediction)))
            print("F1 Score %s - %s" % (str(i), metrics.f1_score( np.ravel(online_news_target), prediction, average='weighted')))
           
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2           
             
if __name__ == '__main__':
    object = Logistic_Regression()