#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn import linear_model, model_selection, metrics
import numpy as np
import math
 
 
# Parameters
learning_rate = 0.01
training_epochs = 1000
display_step = 50
 
# Training Data 70/30 split
data_columns = [1,2,3,4,6,7,8,9,10]
target_columns = [11]
num_rows = 100100
Features = pd.read_csv('The SUM dataset, without noise.csv', delimiter = ";", usecols = data_columns, nrows = num_rows)
Target = pd.read_csv('The SUM dataset, without noise.csv', delimiter = ";", usecols = target_columns, nrows = num_rows)
 
#create training and testing datasets 70-30 split
train_X, test_X, train_Y, test_Y = train_test_split(Features, Target, test_size = 0.3)
print (train_X.shape, train_Y.shape)
print (test_X.shape, test_Y.shape)
#print (train_X)
n_samples = train_X.shape[0]
 
# tf Graph Input
X = tf.placeholder(tf.float32, [None, 9])
Y = tf.placeholder(tf.float32, [None, 1])
 
# Set model weights
W = tf.Variable(tf.ones([9, 1]), name = 'weight')
 
 
# Construct a linear model
pred = tf.matmul(X, W)
 
# Mean squared error
cost = tf.reduce_mean(tf.square(Y-pred))
# Gradient descent
 
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
 
# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()
 
# Start training
with tf.Session() as sess:
 
    # Run the initializer
    sess.run(init)
     
    sess.run(pred,feed_dict={X:train_X} )
    # Fit all training data
    for epoch in range(training_epochs):
        sess.run(optimizer, feed_dict={X: train_X, Y: train_Y})
             
 
        # Display logs per epoch step
        if (epoch+1) % display_step == 0:
           c = sess.run(cost, feed_dict={X: train_X, Y:train_Y})
           #print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c), \
           #     "W=", sess.run(W))
 
    print("Optimization Finished!")
    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    #print (training_cost)
    #print("Training cost=", training_cost, "W=", sess.run(W), '\n')
 
 
     
 
    print("Testing... (Mean square loss Comparison)")
    testing_cost = sess.run(
        tf.reduce_sum(tf.pow(pred - Y, 2)) / (2 * test_X.shape[0]),
        feed_dict={X: test_X, Y: test_Y})  # same function as cost above
    #print("Testing cost=", testing_cost)
    #print("Absolute mean square loss difference:", abs(
    #    training_cost - testing_cost))
    prediction = model_selection.cross_val_predict(linear_model.LinearRegression(), Features, np.ravel(Target), cv=10)
             
    print("RMSE  - %s" % ( math.sqrt(metrics.mean_squared_error( np.ravel(Target), prediction))))
    print("Variance  - %s" % ( metrics.explained_variance_score( np.ravel(Target), prediction)))
     
    print("Done!")