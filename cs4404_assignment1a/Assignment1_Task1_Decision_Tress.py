#!/usr/bin/python 3.6.2
import math
import numpy as np
import pandas as pd
from sklearn import tree, model_selection, metrics
 
class Decision_Tree:
    def __init__(self):
#        self.sum_without_noise()
#        self.sum_with_noise()
#        self.ny_texi()
        self.online_news()
         
    def sum_without_noise(self):
        num_rows = 100
        data_columns = [1,2,3,4,6,7,8,9,10]
        target_columns = [11]
         
        for i in range (9):
            SUM_without_noise_data = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = data_columns, nrows = num_rows)
            SUM_without_noise_target = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = target_columns, nrows = num_rows)
             
#            print(SUM_without_noise_data, SUM_without_noise_target)
            prediction = model_selection.cross_val_predict(tree.DecisionTreeRegressor(), SUM_without_noise_data, np.ravel(SUM_without_noise_target), cv=10)
             
            print("RMSE %s - %s" % (str(i), math.sqrt(metrics.mean_squared_error( np.ravel(SUM_without_noise_target), prediction))))
            print("Variance %s - %s" % (str(i), metrics.explained_variance_score( np.ravel(SUM_without_noise_target), prediction)))
             
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2
                 
    def sum_with_noise(self):
        num_rows = 100
        data_columns = [1,2,3,4,6,7,8,9,10]
        target_columns = [11]
         
        # the 1,000,000 chunk is taking too long to run, skiped it.
        for i in range (9):
            SUM_with_noise_data = pd.read_csv('SUM with noise.csv', delimiter = ";", usecols = data_columns, nrows = num_rows)
            SUM_with_noise_target = pd.read_csv('SUM with noise.csv', delimiter = ";", usecols = target_columns, nrows = num_rows)
             
            prediction = model_selection.cross_val_predict(tree.DecisionTreeRegressor(), SUM_with_noise_data, np.ravel(SUM_with_noise_target), cv=10)
             
            print("RMSE %s - %s" % (str(i),  math.sqrt(metrics.mean_squared_error( np.ravel(SUM_with_noise_target), prediction))))
            print("Variance %s - %s" % (str(i), metrics.explained_variance_score(np.ravel(SUM_with_noise_target), prediction)))
             
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2
                 
    def ny_texi(self):
        num_rows = 100
        data_columns = [5,6,7,8]
        target_columns = [10]
         
        for i in range(10):
            ny_texi_data = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = data_columns,  nrows = num_rows)
            ny_texi_target = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = target_columns,  nrows = num_rows)
                       
            prediction = model_selection.cross_val_predict(tree.DecisionTreeRegressor(), ny_texi_data, np.ravel(ny_texi_target), cv=10)
             
            print("RMSE %s - %s" % (str(i),  math.sqrt(metrics.mean_squared_error( np.ravel(ny_texi_target), prediction))))
            print("Variance %s - %s" % (str(i), metrics.explained_variance_score( np.ravel(ny_texi_target), prediction)))
           
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2
                 
    def online_news(self):
        num_rows = 100
        online_news_pop = pd.read_csv("OnlineNewsPopularity.csv")
        for i in range(6):
            online_news_data = online_news_pop.iloc[:num_rows, 1:-2]
            online_news_target = online_news_pop.iloc[:num_rows, -2]
             
            prediction = model_selection.cross_val_predict(tree.DecisionTreeRegressor(), online_news_data, np.ravel(online_news_target), cv=10)
             
            print("RMSE %s - %s" % (str(i),  math.sqrt(metrics.mean_squared_error( np.ravel(online_news_target), prediction))))
            print("Variance %s - %s" % (str(i), metrics.explained_variance_score( np.ravel(online_news_target), prediction)))
           
            if i%2 == 0:
                num_rows = num_rows*5
            else:
                num_rows = num_rows*2  
                 
if __name__ == '__main__':
    object = Decision_Tree()