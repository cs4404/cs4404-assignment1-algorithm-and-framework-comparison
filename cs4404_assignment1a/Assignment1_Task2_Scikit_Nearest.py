#!/usr/bin/python 3.6.2
import math
import numpy as np
import pandas as pd
from sklearn import neighbors, model_selection, metrics
 
class Nearest_Neighbors:
    def __init__(self):
        self.nearest_neighbor = neighbors.KNeighborsRegressor(n_neighbors=2)
        self.sum_without_noise()
        self.ny_texi()
     
    def sum_without_noise(self):
        data_columns = [1,2,3,4,6,7,8,9,10]
        target_columns = [11]
         
        SUM_without_noise_data = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = data_columns)
        SUM_without_noise_target = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = target_columns)
         
#        SUM_without_noise_data_train, SUM_without_noise_data_test, SUM_without_noise_target_train, SUM_without_noise_target_test = model_selection.train_test_split(SUM_without_noise_data, SUM_without_noise_target, test_size=0.3, random_state=1)
#                
#        self.nearest_neighbor.fit(SUM_without_noise_data_train, np.ravel(SUM_without_noise_target_train))
#        
#        prediction = self.nearest_neighbor.predict(SUM_without_noise_data_test)
         
        prediction = model_selection.cross_val_predict(self.nearest_neighbor, SUM_without_noise_data, np.ravel(SUM_without_noise_target), cv=10)
         
        print("RMSE - %s" % (math.sqrt(metrics.mean_squared_error( np.ravel(SUM_without_noise_target), prediction))))
        print("Variance - %s" % (metrics.explained_variance_score(np.ravel(SUM_without_noise_target), prediction)))
                 
    def ny_texi(self):
        data_columns = [5,6,7,8]
        target_columns = [10]
         
        ny_texi_data = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = data_columns)
        ny_texi_target = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = target_columns)
         
#        ny_texi_data_train, ny_texi_data_test, ny_texi_target_train, ny_texi_target_test = model_selection.train_test_split(ny_texi_data, ny_texi_target, test_size=0.3, random_state=1)
#        
#        self.nearest_neighbor.fit(ny_texi_data_train, np.ravel(ny_texi_target_train))
#        
#        prediction = self.nearest_neighbor.predict(ny_texi_data_test)
         
        prediction = model_selection.cross_val_predict(self.nearest_neighbor, ny_texi_data, np.ravel(ny_texi_target), cv=10)
         
        print("RMSE - %s" % (math.sqrt(metrics.mean_squared_error( np.ravel(ny_texi_target), prediction))))
        print("Variance - %s" % (metrics.explained_variance_score( np.ravel(ny_texi_target), prediction)))
             
if __name__ == '__main__':
    object = Nearest_Neighbors()