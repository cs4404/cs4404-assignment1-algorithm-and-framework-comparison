#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
import tensorflow as tf
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import linear_model, model_selection, metrics
 
#input data
data_columns = [5,6,7,8]
target_columns = [11]
num_rows = 10000
x_input = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = data_columns, nrows = num_rows)
y_input = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = target_columns, nrows = num_rows)
 
#training data 70/30 split
train_X, test_X, train_Y, test_Y = train_test_split(x_input,y_input, test_size = 0.3)
# tf Graph Input
xtr = tf.placeholder("float", [None, 4])
xte = tf.placeholder("float", [4])
 
# Nearest Neighbor calculation using L1 Distance
# Calculate L1 Distance
distance = tf.reduce_sum(tf.abs(tf.add(xtr, tf.negative(xte))), reduction_indices=1)
# Prediction: Get min distance index (Nearest neighbor)
pred = tf.arg_min(distance, 0)
 
accuracy = 0.
 
# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()
 
# Start training
with tf.Session() as sess:
 
    # Run the initializer
    sess.run(init)
 
    # loop over test data
    for i in range(len(test_X)):
        # Get nearest neighbor
        nn_index = sess.run(pred, feed_dict={xtr: train_X, xte: test_X.iloc[i, :]})
        # Get nearest neighbor class label and compare it to its true label
       # print("Test", i, "Prediction:", np.argmax(train_Y.iloc[nn_index]), \
       #     "True Class:", np.argmax(test_Y.iloc[i]))
        # Calculate accuracy
       # if np.argmax(train_Y.iloc[nn_index]) == np.argmax(test_Y.iloc[i]):
       #     accuracy += 1./len(test_X)
     
    prediction = model_selection.cross_val_predict(linear_model.LogisticRegression(), x_input, np.ravel(y_input), cv=10)
    print("Accuracy Score  - %s" % ( metrics.accuracy_score( np.ravel(y_input), prediction)))
    print("F1 Score  - %s" % ( metrics.f1_score( np.ravel(y_input), prediction, average='weighted')))
    print("Done!")