#!/usr/bin/python 3.6.2
import numpy as np
import pandas as pd
import math
from sklearn import linear_model, model_selection, metrics
 
class Linear_Regression:
    def __init__(self):
        self.linear_regression = linear_model.LinearRegression()
        self.sum_without_noise()
        self.ny_texi()
     
    def sum_without_noise(self):
        data_columns = [1,2,3,4,6,7,8,9,10]
        target_columns = [11]
         
        SUM_without_noise_data = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = data_columns)
        SUM_without_noise_target = pd.read_csv('SUM without noise.csv', delimiter = ";", usecols = target_columns)
         
        SUM_without_noise_data_train, SUM_without_noise_data_test, SUM_without_noise_target_train, SUM_without_noise_target_test = model_selection.train_test_split(SUM_without_noise_data, SUM_without_noise_target, test_size=0.3, random_state=1)
                 
        self.linear_regression.fit(SUM_without_noise_data_train, np.ravel(SUM_without_noise_target_train))
         
        prediction = self.linear_regression.predict(SUM_without_noise_data_test)
         
         
        print("RMSE Score - %s" %  math.sqrt(metrics.mean_squared_error( np.ravel(SUM_without_noise_target_test), prediction)))
        print("Variance Score - %s" % (metrics.explained_variance_score( np.ravel(SUM_without_noise_target_test), prediction)))
                 
    def ny_texi(self):
        data_columns = [5,6,7,8]
        target_columns = [10]
         
        ny_texi_data = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = data_columns)
        ny_texi_target = pd.read_csv('NY texi data.csv', delimiter = ",", usecols = target_columns)
         
        ny_texi_data_train, ny_texi_data_test, ny_texi_target_train, ny_texi_target_test = model_selection.train_test_split(ny_texi_data, ny_texi_target, test_size=0.3, random_state=1)
         
        self.linear_regression.fit(ny_texi_data_train, np.ravel(ny_texi_target_train))
         
        prediction = self.linear_regression.predict(ny_texi_data_test)
         
         
        print("RMSE Score - %s" % math.sqrt(metrics.mean_squared_error( np.ravel(ny_texi_target_test), prediction)))
        print("Variance Score - %s" % (metrics.explained_variance_score( np.ravel(ny_texi_target_test), prediction)))
          
             
if __name__ == '__main__':
    object = Linear_Regression()