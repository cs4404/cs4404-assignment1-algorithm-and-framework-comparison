from __future__ import division

import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import GaussianNB

# data files
file1 = 'OnlineNewsPopularity.csv'

clf = GaussianNB()

num_columns = pd.read_csv(file1, sep=',', nrows=1).shape[1]  
length = num_columns

num_columns = pd.read_csv(file1, sep=',', nrows=1).shape[1]  
length = num_columns

# arrays used to store data, targets and predicted values
x = [None]*length
y = [None]*length
yPredict10 = [None]*length
yPredict30 = [None]*length

#training and testing values
xTrain10 = [None]*length
xTest10 = [None]*length
yTrain10 = [None]*length
yTest10 = [None]*length
xTrain30 = [None]*length
xTest30 = [None]*length
yTrain30 = [None]*length
yTest30 = [None]*length

# arrays to hold metrics
accuracy10 = [None]*6
accuracy30 = [None]*6
_f1Score10 = [None]*6
_f1Score30 = [None]*6
NaiveBayes = [clf]*6

data_max = pd.read_csv(file1, delimiter = ';')
# columns needed for predictions for features and targets
featureCols= [' timedelta',' n_tokens_title',' n_tokens_content',
              ' n_unique_tokens' ,' n_non_stop_words',
              ' n_non_stop_unique_tokens',' num_hrefs',' num_self_hrefs',
              ' num_imgs',' num_videos',' average_token_length',' num_keywords',
              ' data_channel_is_lifestyle',' data_channel_is_entertainment',
              ' data_channel_is_bus',' data_channel_is_socmed',
              ' data_channel_is_tech',' data_channel_is_world',' kw_min_min',
              ' kw_max_min',' kw_avg_min',' kw_min_max',' kw_max_max',
              ' kw_avg_max',' kw_min_avg',' kw_max_avg',' kw_avg_avg',
              ' self_reference_min_shares',' self_reference_max_shares',
              ' self_reference_avg_sharess',' weekday_is_monday',
              ' weekday_is_tuesday',' weekday_is_wednesday',
              ' weekday_is_thursday',' weekday_is_friday',
              ' weekday_is_saturday',' weekday_is_sunday',' is_weekend',
              ' LDA_00',' LDA_01',' LDA_02',' LDA_03',' LDA_04',
              ' global_subjectivity',' global_sentiment_polarity', 
              ' global_rate_positive_words',' global_rate_negative_words', 
              ' rate_positive_words',' rate_negative_words',' avg_positive_polarity', 
              ' min_positive_polarity',' max_positive_polarity',
              ' avg_negative_polarity',' min_negative_polarity', 
              ' max_negative_polarity',' title_subjectivity', 
              ' title_sentiment_polarity',' abs_title_subjectivity', 
              ' abs_title_sentiment_polarity']
targetCols= ['Target Label']
numbers = [100, 500, 1000, 5000, 10000, 50000]

#read data in from file
def getData(fileX):
    _nrows = 100
    for i in range(0, 6):
        x[i] = pd.read_csv(fileX, delimiter = ',', nrows = _nrows, usecols = featureCols)
        #print x[i]
        x[i]=x[i].values

        y[i] = pd.read_csv(fileX, delimiter = ',', nrows = _nrows, usecols = targetCols)
        #print y[i]
        y[i]=y[i].values
        if(_nrows == 50000):
            y[i]=y[i].reshape(39643,)
        else:
            y[i]=y[i].reshape(_nrows,)
        if i%2 == 0:
            _nrows = _nrows*5
        else:
            _nrows = _nrows*2
# end of function
             
def setTestAndTrainVals():
    for k in range(0, 6):
        xTrain30[k], xTest30[k], yTrain30[k], yTest30[k] = train_test_split(x[k], y[k],
              test_size=0.30, shuffle = False)
        xTrain10[k], xTest10[k], yTrain10[k], yTest10[k] = train_test_split(x[k], y[k],
              test_size=0.30, shuffle = False)
        print "getting train/test data", k
        #print yTest[k].T
# end of function
        
def fitNaiveBayes():
    #print xTrain[0]
    for i in range(0,6):
        NaiveBayes[i] = clf.fit(xTrain30[i], yTrain30[i])
        print "fitting Baye", i
    # end of function

def baesPredictions():
     for i in range(0,6):         
         print "baes  prediction", i
         yPredict30[i] = NaiveBayes[i].predict(xTest30[i])
         yPredict10[i] = cross_val_predict(clf, x[i], y[i], cv=10) 
         #print yPredict10
         #print yPredict30
  
def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def crossValidation():
    for j in range (0, 6):
        	## cross validation score
	    print 'Cross validation score', j
            accuracy10[j] = accuracy_score(y[j], yPredict10[j])
            accuracy30[j] = accuracy_score(yTest30[j], yPredict30[j])
            #accuracy10[j] = mean(accuracy10[j])
            #accuracy30[j] = mean(accuracy30[j])



def f1Score():
    for i in range(0, 6):
        _f1Score10[i]  = f1_score(y[i], yPredict10[i], average='weighted')
        print "f1 score: ", i, _f1Score10[i]
        _f1Score30[i]  = f1_score(yTest30[i], yPredict30[i], average='weighted')
        print "f1 score: ", i, _f1Score30[i]
            
getData(file1)
print "got Data"
setTestAndTrainVals()
print "set test and train vals"
fitNaiveBayes()
print "fitted bae"
baesPredictions()
print "bae made her predictions"
crossValidation()
print "got cross validation"
f1Score()
print "finished with Bae"

#print _f1Score
print accuracy10
print _f1Score10
print accuracy30
print _f1Score30

plt.plot(numbers, _f1Score10, color='blue', linewidth=3)
plt.plot(numbers, _f1Score30, color='grey', linewidth=3)
plt.plot(numbers, accuracy30,  color='pink', linewidth=4)
plt.plot(numbers, accuracy10,  color='black', linewidth=4)
#plt.plot(numbers, R2, color='green', linewidth=3)
##print len(numbers)
#print len(RMSE)
plt.xticks()
plt.yticks()

plt.show()
#print RMSE