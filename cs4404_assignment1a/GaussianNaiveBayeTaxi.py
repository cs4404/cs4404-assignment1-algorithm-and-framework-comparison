from __future__ import division

import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score,  accuracy_score
from sklearn.model_selection import cross_val_predict

import pandas as pd
from sklearn.naive_bayes import GaussianNB

# data files
file1 = 'NY texi data.csv'

clf = GaussianNB()

num_columns = pd.read_csv(file1, sep=';', nrows=1).shape[1]  
length = num_columns
length = num_columns

num_columns = pd.read_csv(file1, sep=',', nrows=1).shape[1]  
length = num_columns

# arrays used to store data, targets and predicted values
x = [None]*length
y = [None]*length
yPredict10 = [None]*length
yPredict30 = [None]*length

#training and testing values
xTrain10 = [None]*length
xTest10 = [None]*length
yTrain10 = [None]*length
yTest10 = [None]*length
xTrain30 = [None]*length
xTest30 = [None]*length
yTrain30 = [None]*length
yTest30 = [None]*length

# arrays to hold metrics
accuracy10 = [None]*6
accuracy30 = [None]*6
_f1Score10 = [None]*6
_f1Score30 = [None]*6
NaiveBayes = [clf]*6


# columns needed for predictions for features and targets
# columns needed for predictions for features and targets
featureCols= ['dropoff_longitude','dropoff_latitude','pickup_longitude',
              'pickup_latitude','passenger_count']
targetCols= ['trip_duration']
numbers = [100, 500, 1000, 5000, 10000, 50000]

NaiveBayes = [clf]*6

#read data in from file
def getData():
    _nrows = 100
    for i in range(0, 6):
        x[i] = pd.read_csv(file1, delimiter = ',', nrows = _nrows, usecols = featureCols)
        #print x[i]
        x[i]=x[i].values

        y[i] = pd.read_csv(file1, delimiter = ',', nrows = _nrows, usecols = targetCols)
        #print y[i]
        y[i]=y[i].values
        y[i]=y[i].reshape(_nrows,)
        if i%2 == 0:
            _nrows = _nrows*5
        else:
            _nrows = _nrows*2
# end of function
             
def setTestAndTrainVals():
    for k in range(0, 6):
        xTrain30[k], xTest30[k], yTrain30[k], yTest30[k] = train_test_split(x[k], y[k],
              test_size=0.30, shuffle = False)
        xTrain10[k], xTest10[k], yTrain10[k], yTest10[k] = train_test_split(x[k], y[k],
              test_size=0.10, shuffle = False)
        print "getting train/test data", k
        #print yTest[k].T
# end of function
        
def fitNaiveBayes():
    #print xTrain[0]
    for i in range(0,6):
        NaiveBayes[i] = clf.fit(xTrain30[i], yTrain30[i])
        print "fitting Baye", i
    # end of function

def baesPredictions():
     for i in range(0,6):         
         print "baes  prediction", i
         yPredict30[i] = NaiveBayes[i].predict(xTest10[i])
         #yPredict10[i] = cross_val_predict(clf, x[i], np.ravel(y[i]), cv=10) 
         #print yPredict10
         #print yPredict30  
  
def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def crossValidation():
    for j in range (0, 6):
        	## cross validation score
	    print 'Cross validation score', j
            #accuracy10[j] = accuracy_score(y[j], yPredict10[j])
            accuracy30[j] = accuracy_score(yTest10[j], yPredict30[j])
            #accuracy10[j] = mean(accuracy10[j])
            #accuracy30[j] = mean(accuracy30[j])



def f1Score():
    for i in range(0, 6):
        #_f1Score10[i]  = f1_score(y[i], yPredict10[i], average='weighted')
        print "f1 score: ", i, _f1Score10[i]
        _f1Score30[i]  = f1_score(yTest10[i], yPredict30[i], average='weighted')
        print "f1 score: ", i, _f1Score30[i]
            
getData()
print "got Data"
setTestAndTrainVals()
print "set test and train vals"
fitNaiveBayes()
print "fitted bae"
baesPredictions()
print "bae made her predictions"
crossValidation()
print "got cross validation"
f1Score()
print "finished with Bae"

#print _f1Score
print accuracy10
print _f1Score10
print accuracy30
print _f1Score30

plt.plot(numbers, _f1Score10, color='blue', linewidth=3)
plt.plot(numbers, _f1Score30, color='grey', linewidth=3)
plt.plot(numbers, accuracy30,  color='pink', linewidth=4)
plt.plot(numbers, accuracy10,  color='black', linewidth=4)
#plt.plot(numbers, R2, color='green', linewidth=3)
##print len(numbers)
#print len(RMSE)
plt.xticks()
plt.yticks()

plt.show()
#print RMSE